#pragma once

#include <concepts>

#include <fmt/core.h>

namespace Error {

class Base : public std::exception {
public:
	explicit Base(std::string msg) : msg(std::move(msg)) {}

	[[nodiscard]] const char* what() const noexcept override {
		return msg.c_str();
	}

private:
	std::string msg;
};

//template <typename A>
//concept FromBase = std::derived_from<A, Base>;

template <typename E>
class Formatter {
public:
	template <typename... Args>
	static inline E formatted(const std::string& fmtStr, Args&&... args) {
		return E(fmt::format(fmt::runtime(fmtStr), std::forward<Args>(args)...));
	}
};

template <typename A>
class BaseOf : public Base, public Formatter<A> {
public:
	template <typename... Args>
	explicit BaseOf(Args&&... args) : Base(std::forward<Args>(args)...) {}
};

struct Argument : public BaseOf<Argument> {
	explicit Argument(std::string msg) : BaseOf(std::move(msg)) {}
};

struct IO : public BaseOf<IO> {
	explicit IO(std::string msg) : BaseOf(std::move(msg)) {}
};

struct Runtime : public BaseOf<Runtime> {
	explicit Runtime(std::string msg) : BaseOf(std::move(msg)) {}
};

struct Internal : public BaseOf<Internal> {
	explicit Internal(std::string msg) : BaseOf(std::move(msg)) {}
};

struct NotImplemented : public BaseOf<NotImplemented> {
	explicit NotImplemented(std::string msg) : BaseOf(std::move(msg)) {}
};

struct Lexer : public BaseOf<Lexer> {
	explicit Lexer(std::string msg) : BaseOf(std::move(msg)) {}
};

struct Parser : public BaseOf<Parser> {
	explicit Parser(std::string msg) : BaseOf(std::move(msg)) {}
};

}