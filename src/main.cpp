#include <iostream>
#include <optional>
#include <fstream>

#include <cxxopts.hpp>
#include <spdlog/spdlog.h>

#include "errors/errors.h"
#include "compiledb.h"

static const char* ARG_POSITIONAL = "positional";
static const char* ARG_OUTPUT = "output";
static const char* ARG_HELP = "help";
static const char* ARG_VERBOSE = "verbose";
static const char* ARG_LEX = "lex";

struct Context {
	std::vector<std::string> files;
	bool forceQuit = false;
	bool verbose = false;
	bool onlyLexer = false;
};

template <typename A>
std::optional<A> asOptional(const cxxopts::ParseResult& r, const char* name) {
	try {
		if (r[name].count() > 0) {
			return r[name].as<A>();
		} else {
			return std::nullopt;
		}
	} catch (const std::bad_cast& e) {
		throw Error::Argument::formatted("Unexpected type of value for option `{}`.", name);
	}
}

template <typename A>
std::vector<A> asArray(const cxxopts::ParseResult& r, const char* name) {
	auto arr = asOptional<std::vector<A>>(r, name);
	if (!arr)
		return std::vector<A>();
	else {
		return *arr;
	}
}

Context createContext(int argc, char** argv) {
	using std::vector;
	using std::string;

	cxxopts::Options options("pl0c", "A research PL/0 compiler");
	options.parse_positional({ ARG_POSITIONAL });
	options
		.positional_help("[files to compile...]")
		.add_options("General")
			(fmt::format("o,{}", ARG_OUTPUT), "Output binary object", cxxopts::value<string>())
			(fmt::format("l,{}", ARG_LEX), "Only print lexer information and quit")
			(fmt::format("h,{}", ARG_HELP), "This help screen")
			(fmt::format("v,{}", ARG_VERBOSE), "Use verbose logging")
			(ARG_POSITIONAL, "Source files to compile", cxxopts::value<vector<string>>())
	;

	try {
		auto result = options.parse(argc, argv);
		if (result.count(ARG_HELP) > 0) {
			fmt::print("{}", options.help());
			return Context { .forceQuit = true };
		}

		return Context {
			.files = asArray<string>(result, ARG_POSITIONAL),
			.verbose = asOptional<bool>(result, ARG_VERBOSE).has_value(),
			.onlyLexer = asOptional<bool>(result, ARG_LEX).has_value(),
		};
	} catch (cxxopts::OptionException& e) {
		throw Error::Argument(e.what());
	}
}

std::string readCode(const std::string& path) {
	std::ifstream ifs(path);
	if (!ifs) {
		throw Error::IO::formatted("error opening {}", path);
	}

	std::vector<char> buf;
	buf.resize(4096);
	std::string code;

	while (ifs.good()) {
		ifs.read(buf.data(), 4096);
		code += buf.data();
	}

	return code;
}

void compile(const Context& c, ProgramDB& cdb, int id) {
	const std::string& src = cdb.getSourceCode(id);
	auto lxc = LexerContext(src);
	cdb.compile(lxc);
}

int run(const Context& c) {
	ProgramDB cdb;
	std::vector<int> idsToCompile;

	for (const auto& path : c.files) {
		if (!path.ends_with(".pl0")) {
			throw Error::Argument::formatted("filename doesn't end with a known extension: '{}' (expected '.pl0')", path);
		}

		int id = cdb.allocateIdForPath(path);
		idsToCompile.push_back(id);

		auto sourceCode = readCode(path);
		cdb.setSourceCode(id, sourceCode);

		spdlog::trace("Allocated id={}, size={}, path={}", id, sourceCode.size(), path);
	}

	if (c.onlyLexer) {
		for (auto id: idsToCompile) {
			spdlog::trace("Lexing file {}...", id);
			const std::string& src = cdb.getSourceCode(id);
			auto lxc = LexerContext(src);
			while (true) {
				auto t = cdb.lex(lxc);
				if (!t) break;

				spdlog::info("Token: {}", t->toString());
			}
		}
	} else {
		for (auto id: idsToCompile) {
			spdlog::trace("Compiling file {}...", id);
			compile(c, cdb, id);
		}
	}

	return 0;
}

int main(int argc, char** argv) {
	try {
		auto ctx = createContext(argc, argv);
		if (ctx.files.empty()) {
			spdlog::info("Please use `--help` to get help about usage.");
			return 1;
		}

		if (ctx.forceQuit) {
			return 0;
		}

		if (ctx.verbose) {
			spdlog::default_logger()->set_level(spdlog::level::trace);
		}

		return run(ctx);
	} catch (const Error::Argument& e) {
		spdlog::error("Error in command line: {}", e.what());
		spdlog::error("Use `--help` to get help about usage.");
		return 1;
	} catch (const Error::IO& e) {
		spdlog::error("I/O error, can't continue: {}", e.what());
		return 1;
	} catch (const Error::Lexer& e) {
		spdlog::error("Compilation error: {}", e.what());
		return 1;
	}

	return 0;
}
