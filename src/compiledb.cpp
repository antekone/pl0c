#include <spdlog/spdlog.h>

#include "compiledb.h"

void unimplemented() {
	throw Error::NotImplemented("not implemented yet");
}

std::optional<Token> ProgramDB::lex(LexerContext& lxc) {
	while (true) {
		auto ch = lxc.next();
		if (!ch) {
			return Token(TokenType::eof, lxc.line, lxc.column, lxc.id);
		}

		if (*ch == ' ' || *ch == '\t' || *ch == '\r')
			continue;

		if (*ch == '\n') {
			lxc.line++;
			lxc.column = 1;
			continue;
		}

		if (std::isdigit(*ch)) {
			lxc.unget(*ch);
			return lexNumber(lxc);
		}

		if (std::isalpha(*ch)) {
			lxc.unget(*ch);
			return lexIdentifier(lxc);
		}

		switch (*ch) {
			case '=': return Token(TokenType::equal, lxc.line, lxc.column, lxc.id);
			case ',': return Token(TokenType::comma, lxc.line, lxc.column, lxc.id);
			case ';': return Token(TokenType::semicolon, lxc.line, lxc.column, lxc.id);
			case '<': return Token(TokenType::lessThan, lxc.line, lxc.column, lxc.id);
			case '>': return Token(TokenType::greaterThan, lxc.line, lxc.column, lxc.id);
			case '/': return Token(TokenType::divide, lxc.line, lxc.column, lxc.id);
			case '*': return Token(TokenType::multiply, lxc.line, lxc.column, lxc.id);
			case '+': return Token(TokenType::plus, lxc.line, lxc.column, lxc.id);
			case '-': return Token(TokenType::minus, lxc.line, lxc.column, lxc.id);
			case '.': return Token(TokenType::dot, lxc.line, lxc.column, lxc.id);
			case '(': return Token(TokenType::lparen, lxc.line, lxc.column, lxc.id);
			case ')': return Token(TokenType::rparen, lxc.line, lxc.column, lxc.id);
			case '#': return Token(TokenType::hash, lxc.line, lxc.column, lxc.id);
			case ':': {
				auto ch2 = lxc.next();
				if (!ch2) break;
				if (*ch2 != '=') {
					throw Error::Lexer::formatted("Expected operator := at line {}, column {}", lxc.line, lxc.column - 1);
				}

				return Token(TokenType::assign, lxc.line, lxc.column, lxc.id);
			}

			default: break;
		}

		throw Error::Lexer::formatted("Invalid token at line {}, column {}", lxc.line, lxc.column - 1);
	}

	return {};
}

Token ProgramDB::lexNumber(LexerContext& lxc) {
	std::string buf;
	bool hex = false;

	while (true) {
		auto ch = lxc.next();
		if (!ch)
			throw Error::IO::formatted("premature end of file when parsing a number");

		if (*ch >= '0' && *ch <= '9') {
			buf += *ch;
		} else if (buf.size() == 1 && buf[0] == '0' && (*ch == 'x' || *ch == 'X')) {
			buf.clear();
			hex = true;
		} else {
			lxc.unget(*ch);
			break;
		}
	}

	auto num = std::stoul(buf, nullptr, hex ? 16 : 10);
	return Token(TokenType::number, num, lxc.line, lxc.column, lxc.id);
}

Token ProgramDB::lexIdentifier(LexerContext& lxc) {
	std::string buf;

	while (true) {
		auto ch = lxc.next();
		if (!ch)
			throw Error::IO::formatted("premature end of file when parsing identifier");

		if (std::isalnum(*ch) || *ch == '_') {
			buf += *ch;
		} else {
			lxc.unget(*ch);
			break;
		}
	}

	if (buf == "const") {
		return Token(TokenType::const_, lxc.line, lxc.column, lxc.id);
	} else if (buf == "var") {
		return Token(TokenType::var, lxc.line, lxc.column, lxc.id);
	} else if (buf == "procedure") {
		return Token(TokenType::procedure, lxc.line, lxc.column, lxc.id);
	} else if (buf == "call") {
		return Token(TokenType::call, lxc.line, lxc.column, lxc.id);
	} else if (buf == "read") {
		return Token(TokenType::read, lxc.line, lxc.column, lxc.id);
	} else if (buf == "write") {
		return Token(TokenType::write, lxc.line, lxc.column, lxc.id);
	} else if (buf == "begin") {
		return Token(TokenType::begin, lxc.line, lxc.column, lxc.id);
	} else if (buf == "end") {
		return Token(TokenType::end, lxc.line, lxc.column, lxc.id);
	} else if (buf == "if") {
		return Token(TokenType::if_, lxc.line, lxc.column, lxc.id);
	} else if (buf == "then") {
		return Token(TokenType::then, lxc.line, lxc.column, lxc.id);
	} else if (buf == "while") {
		return Token(TokenType::while_, lxc.line, lxc.column, lxc.id);
	} else if (buf == "do") {
		return Token(TokenType::do_, lxc.line, lxc.column, lxc.id);
	} else if (buf == "odd") {
		return Token(TokenType::odd, lxc.line, lxc.column, lxc.id);
	} else {
		return Token(TokenType::ident, buf, lxc.line, lxc.column, lxc.id);
	}
}

void ProgramDB::compile(LexerContext& lxc) {
	spdlog::trace("Compilation start");

	next(lxc);
	expectBlock(lxc);
	std::ignore = expectToken(lxc, TokenType::dot);

	spdlog::trace("Compilation end");
}

Token ProgramDB::next(LexerContext& lxc) {
	auto t = lex(lxc);
	if (!t) {
		throw Error::Parser::formatted("{}: expected a token, got EOF instead", lxc.position());
	}

	spdlog::trace("    -> next token: {}", t->toString());

	currentToken = t;
	return *t;
}

void ProgramDB::expectBlock(LexerContext& lxc) {
	auto t = current();

	if (t.type == TokenType::const_) {
		expectConst(lxc);
		t = current();
	}

	if (t.type == TokenType::var) {
		expectVar(lxc);
		t = current();
	}

	while (t.type == TokenType::procedure) {
		expectProcedure(lxc);
		t = current();
	}

	expectStatement(lxc);
}

Token ProgramDB::expectToken(LexerContext& lxc, TokenType requestedType) {
	auto t = current();
	if (t.type != requestedType) {
		throw Error::Parser::formatted("{}: expected '{}', but got '{}'",
									   lxc.position(),
									   magic_enum::enum_name(requestedType),
									   magic_enum::enum_name(t.type));
	} else {
		return t;
	}
}

void ProgramDB::expectTerm(LexerContext& lxc) {
	spdlog::trace("entering term");
	expectFactor(lxc);

	auto t = current();
	while (t.type == TokenType::multiply || t.type == TokenType::divide) {
		std::ignore = next(lxc);
		expectFactor(lxc);
		t = current();
	}
}

void ProgramDB::expectFactor(LexerContext& lxc) {
	spdlog::trace("entering factor");
	auto t = current();

	if (t.type == TokenType::ident) {
		auto ident = expectToken(lxc, TokenType::ident);
		spdlog::trace("ident '{}'", ident.renderVariant());
		std::ignore = next(lxc);
	} else if (t.type == TokenType::number) {
		auto number = expectToken(lxc, TokenType::number);
		spdlog::trace("number '{}'", number.renderVariant());
		std::ignore = next(lxc);
	} else if (t.type == TokenType::lparen) {
		spdlog::trace("(");
		std::ignore = expectToken(lxc, TokenType::lparen);
		std::ignore = next(lxc);
		expectExpression(lxc);
		spdlog::trace(")");
		std::ignore = expectToken(lxc, TokenType::rparen);
		std::ignore = next(lxc);
	} else {
		throw Error::Parser::formatted("{}: expected ident, number or '('", lxc.position());
	}
}

void ProgramDB::expectExpression(LexerContext& lxc) {
	spdlog::trace("entering expression");

	auto t = current();
	if (t.type == TokenType::plus) {
		spdlog::trace("+");
		std::ignore = next(lxc);
	} else if (t.type == TokenType::minus) {
		spdlog::trace("-");
		std::ignore = next(lxc);
	}

	expectTerm(lxc);

	t = current();
	while (t.type == TokenType::plus || t.type == TokenType::minus) {
		std::ignore = next(lxc);
		expectTerm(lxc);
		t = current();
	}
}

void ProgramDB::expectCondition(LexerContext& lxc) {
	spdlog::trace("entering condition");
	auto t = current();
	if (t.type == TokenType::odd) {
		spdlog::trace("entering odd");
		std::ignore = next(lxc);
		expectExpression(lxc);
	} else {
		expectExpression(lxc);

		t = current();

		if (t.type == TokenType::greaterThan) {
			spdlog::trace(">=");
		} else if (t.type == TokenType::lessThan) {
			spdlog::trace("<=");
		} else if (t.type == TokenType::hash) {
			spdlog::trace("#");
		} else if (t.type == TokenType::equal) {
			spdlog::trace("=");
		} else {
			throw Error::Parser::formatted("{}: unknown operator for 'if'", lxc.position());
		}

		std::ignore = next(lxc);
		expectExpression(lxc);
	}
}

void ProgramDB::expectStatement(LexerContext& lxc) {
	spdlog::trace("entering statement");
	auto t = current();
	if (t.type == TokenType::ident) {
		spdlog::trace("entering ident");
		auto ident = expectToken(lxc, TokenType::ident);
		std::ignore = next(lxc);
		std::ignore = expectToken(lxc, TokenType::assign);
		std::ignore = next(lxc);
		expectExpression(lxc);
		spdlog::trace("assign: {} <- expression", ident.renderVariant());
	} else if (t.type == TokenType::call) {
		spdlog::trace("entering call");
		std::ignore = expectToken(lxc, TokenType::call);
		std::ignore = next(lxc);
		auto ident = expectToken(lxc, TokenType::ident);
		std::ignore = next(lxc);
		spdlog::trace("call {}", ident.renderVariant());
	} else if (t.type == TokenType::write) {
		spdlog::trace("entering write");
		std::ignore = next(lxc);
		auto ident = expectToken(lxc, TokenType::ident);
		std::ignore = next(lxc);
		spdlog::trace("call {}", ident.renderVariant());
	} else if (t.type == TokenType::read) {
		spdlog::trace("entering read");
		std::ignore = next(lxc);
		auto ident = expectToken(lxc, TokenType::ident);
		std::ignore = next(lxc);
		spdlog::trace("call {}", ident.renderVariant());
	} else if (t.type == TokenType::begin) {
		spdlog::trace("entering begin");
		std::ignore = next(lxc);
		expectStatement(lxc);
		auto maybeSemicolon = current();
		while (maybeSemicolon.type == TokenType::semicolon) {
			std::ignore = next(lxc);
			expectStatement(lxc);
			maybeSemicolon = current();
		}

		std::ignore = expectToken(lxc, TokenType::end);
		std::ignore = next(lxc);
	} else if (t.type == TokenType::if_) {
		spdlog::trace("entering if");
		std::ignore = next(lxc);
		expectCondition(lxc);
		std::ignore = expectToken(lxc, TokenType::then);
		std::ignore = next(lxc);
		expectStatement(lxc);
	} else if (t.type == TokenType::while_) {
		spdlog::trace("entering while");
		std::ignore = next(lxc);
		expectCondition(lxc);
		std::ignore = expectToken(lxc, TokenType::do_);
		std::ignore = next(lxc);
		expectStatement(lxc);
	}
}

void ProgramDB::expectProcedure(LexerContext& lxc) {
	std::ignore = expectToken(lxc, TokenType::procedure);
	std::ignore = next(lxc);
	auto ident = expectToken(lxc, TokenType::ident);
	std::ignore = next(lxc);
	std::ignore = expectToken(lxc, TokenType::semicolon);
	std::ignore = next(lxc);

	spdlog::trace("procedure: {}", ident.renderVariant());

	expectBlock(lxc);
	std::ignore = expectToken(lxc, TokenType::semicolon);
	std::ignore = next(lxc);
}

void ProgramDB::expectVar(LexerContext& lxc) {
	// [ "var" ident {"," ident} ";"]

	std::ignore = next(lxc);
	auto ident = expectToken(lxc, TokenType::ident);
	auto maybeComma = next(lxc);

	spdlog::trace("var: {}", ident.renderVariant());

	while (maybeComma.type == TokenType::comma) {
		std::ignore = next(lxc);
		auto ident = expectToken(lxc, TokenType::ident);
		spdlog::trace("var: {}", ident.renderVariant());
		maybeComma = next(lxc);
	}

	std::ignore = expectToken(lxc, TokenType::semicolon);
	std::ignore = next(lxc);
}

void ProgramDB::expectConst(LexerContext& lxc) {
	// [ "const" ident "=" number {"," ident "=" number} ";"]
	std::ignore = next(lxc);
	auto ident = expectToken(lxc, TokenType::ident);
	std::ignore = next(lxc);
	std::ignore = expectToken(lxc, TokenType::equal);
	std::ignore = next(lxc);
	auto value = expectToken(lxc, TokenType::number);

	spdlog::trace("const: {} <- {}", ident.renderVariant(), value.renderVariant());

	auto maybeComma = next(lxc);
	while (maybeComma.type == TokenType::comma) {
		std::ignore = next(lxc);
		auto ident = expectToken(lxc, TokenType::ident);
		std::ignore = next(lxc);
		std::ignore = expectToken(lxc, TokenType::equal);
		std::ignore = next(lxc);
		auto value = expectToken(lxc, TokenType::number);

		spdlog::trace("const: {} <- {}", ident.renderVariant(), value.renderVariant());

		maybeComma = next(lxc);
	}

	std::ignore = expectToken(lxc, TokenType::semicolon);
	std::ignore = next(lxc);
}

Token& ProgramDB::current() {
	if (!currentToken) {
		throw Error::Parser::formatted("premature end of file");
	} else {
		return *currentToken;
	}
}
