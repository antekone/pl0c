#pragma once

#include <string>
#include <map>
#include <variant>
#include <optional>
#include <deque>
#include <sstream>

#include <magic_enum.hpp>

#include "errors/errors.h"

enum class TokenType {
	ident, number, const_, var, procedure, call, begin, end,
	if_, then, while_, do_, odd, dot, equal, comma, semicolon,
	assign, hash, lessThan, greaterThan, plus, minus, multiply,
	divide, lparen, rparen, write, read, eof
};

class Token {
public:
	TokenType type;
	std::optional<std::variant<uint8_t, uint64_t, std::string>> data;
	int line;
	int col;
	int fileId;

	template <typename A>
	explicit Token(TokenType type, A arg, int line, int col, int fileId) :
		type(type), data(arg), line(line), col(col), fileId(fileId)
	{}

	explicit Token(TokenType type, int line, int col, int fileId) :
		type(type), data{}, line(line), col(col), fileId(fileId)
	{}

	explicit Token(TokenType type, std::string arg, int line, int col, int fileId) :
		type(type), data(std::move(arg)), line(line), col(col), fileId(fileId)
	{}

	[[nodiscard]] std::string renderVariant() const {
		if (auto c = std::get_if<uint8_t>(&*data); c) {
			return fmt::format("0x{:02X}", *c);
		} else if (auto n = std::get_if<uint64_t>(&*data); n) {
			return fmt::format("0x{:08X}", *n);
		} else if (auto s = std::get_if<std::string>(&*data); s) {
			return fmt::format("'{}'", *s);
		} else {
			throw Error::Internal::formatted("Unsupported token type");
		}
	}

	[[nodiscard]] std::string toString() const {
		std::ostringstream str;
		str << "Token[type=" << magic_enum::enum_name(type);

		if (data) {
			auto dataStr = renderVariant();
			str << ", data=" << dataStr;
		}

		str << ", line=" << line << ", col=" << col << ", file=" << fileId << "]";
		return str.str();
	}
};

struct LexerContext {
	int line = 1;
	int column = 1;
	int id = 0;
	size_t ptr = 0;

	const std::string& source;
	std::deque<char> queue;

	explicit LexerContext(const std::string& src) : source(src) { }

	std::optional<char> next() {
		if (!queue.empty()) {
			auto ch = queue.front();
			queue.pop_front();
			column++;
			return ch;
		}

		if (ptr < source.size()) {
			column++;
			return source[ptr++];
		} else {
			return {};
		}
	}

	void unget(char ch) {
		column--;
		queue.push_front(ch);
	}

	std::string position() {
		return fmt::format("[{}:{} - {}]", line, column, id);
	}
};

class ProgramDB {
public:
	int allocateIdForPath(std::string path) {
		int id = nextId++;
		fileIds.emplace(id, std::move(path));
		return id;
	}

	void setSourceCode(int id, std::string sourceCode) {
		sourceCodes.emplace(id, std::move(sourceCode));
	}

	const std::string& getSourceCode(int id) {
		const auto f = sourceCodes.find(id);
		if (f == sourceCodes.end()) {
			throw Error::Internal::formatted("Missing source code for requested id");
		}

		return f->second;
	}

	[[nodiscard]] const auto& getFileIds() const { return fileIds; }
	[[nodiscard]] const auto& getSourceCodes() const { return sourceCodes; }

	void compile(LexerContext& lxc);
	Token next(LexerContext& lxc);
	std::optional<Token> lex(LexerContext& lxc);

private:
	std::map<int, std::string> fileIds;
	std::map<int, std::string> sourceCodes;
	int nextId = 0;

	std::optional<Token> currentToken;
	Token lexNumber(LexerContext& lxc);
	Token lexIdentifier(LexerContext& lxc);

	Token& current();
	void expectBlock(LexerContext& lxc);
	[[nodiscard]] Token expectToken(LexerContext& lxc, TokenType type);
	void expectConst(LexerContext& lxc);
	void expectVar(LexerContext& lxc);
	void expectProcedure(LexerContext& lxc);
	void expectStatement(LexerContext& lxc);
	void expectExpression(LexerContext& lxc);
	void expectTerm(LexerContext& lxc);
	void expectFactor(LexerContext& lxc);
	void expectCondition(LexerContext& lxc);
};
